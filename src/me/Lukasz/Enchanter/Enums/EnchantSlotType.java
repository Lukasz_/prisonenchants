package me.Lukasz.Enchanter.Enums;

public enum EnchantSlotType
{

    HELMET(),
    CHESTPLATE(),
    LEGGINGS(),
    BOOTS(),
    SWORD(),
    AXE,
    FISHING_ROD,
    PICKAXE();

}
