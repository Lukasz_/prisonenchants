package me.Lukasz.Enchanter.Enums;

import me.Lukasz.Utils.C;
import org.bukkit.ChatColor;

public enum EnchantRarity
{

    //RARITY-Color-Bold
    GODLY(ChatColor.RED, true),
    LEGENDRY(ChatColor.GOLD, false),
    ELITE(ChatColor.BLUE, false),
    UNIQUE(ChatColor.YELLOW, false),
    COMMON(ChatColor.GREEN, false);


    private ChatColor color;
    private boolean bold;

    EnchantRarity(ChatColor color1, boolean bold1)
    {
        color = color1;
        bold = bold1;
    }

    public String getRarityColorFull()
    {
        return isBold() ? getColor() + C.Bold : getColor() + "";
    }

    public ChatColor getColor()
    {
        return color;
    }

    public boolean isBold()
    {
        return bold;
    }
}
