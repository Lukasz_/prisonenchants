package me.Lukasz.Enchanter.Enums;

public enum EnchantList
{
    //Common
    FEED("Feed", 3, EnchantRarity.COMMON, EnchantSlotType.PICKAXE, "Whilst mining, has a chance to replenish hunger!"), 
    XP_MAGNET("XP Magnet", 5, EnchantRarity.COMMON, EnchantSlotType.PICKAXE, "Chance to add more XP per block mined!"), 
    HYPER("Hyper", 3, EnchantRarity.COMMON, EnchantSlotType.PICKAXE, "Chance to add speed to player, allowing them to mine faster!"), 
    //GLOW("Glow", 1, EnchantRarity.COMMON, EnchantSlotType.HELMET, "Gives Night Vision effect"),
    //LEAP("Leap", 3, EnchantRarity.COMMON, EnchantSlotType.BOOTS, "Gives Jump Boost Effect [Higher Level, Higher Effect]"),
    //Unique
    ADRENALIN("Adrenalin", 3, EnchantRarity.UNIQUE, EnchantSlotType.PICKAXE, "Chance to add haste to player, allowing them to mine faster!"), 
    COIN_MAGNET("Coin Magnet", 3, EnchantRarity.UNIQUE, EnchantSlotType.PICKAXE, "Increases the chance of finding Money through mining!"), 
    DUST_CATCHER("Dust Catcher", 6, EnchantRarity.UNIQUE, EnchantSlotType.FISHING_ROD, "Adds the ability to catch Star Dust through fishing!"), 
    //Elite
    FORTUNE("Fortune", 10, EnchantRarity.ELITE, EnchantSlotType.PICKAXE, "Chance to get more blocks through mining!"), 
    DUST_COLLECTOR("Dust Collector", 10, EnchantRarity.ELITE, EnchantSlotType.PICKAXE, "Increases the chance of finding Star Dust through mining!"), 
    STAR_CATCHER("Star Catcher", 4, EnchantRarity.ELITE, EnchantSlotType.FISHING_ROD, "Adds the ability to catch Stars through fishing!"),
    //MOLTEN("Molten", 6, EnchantRarity.ELITE, EnchantSlotType.CHESTPLATE, "Chance to set opponent on Fire!"),
    //Legendary
    LUCK("Lucky", 5, EnchantRarity.LEGENDRY, EnchantSlotType.PICKAXE, "Chance to find Money, Star Dust, and Stars through mining! (Multiple finds can happen)!"), 
    LUCK_OF_THE_SEA("Luck Of The Sea",5, EnchantRarity.LEGENDRY, EnchantSlotType.FISHING_ROD, "Chance to get other items from fishing!"),
    //Godly
    EXCAVATION("Excavation", 1, EnchantRarity.GODLY, EnchantSlotType.PICKAXE, "Allows to mine any block, as long as you are high enough level!");

    private String name;
    private int MaxLevel;
    private EnchantRarity rarity;
    private EnchantSlotType slotType;
    private String desc;

    EnchantList(String name1, int MaxLevel1, EnchantRarity rarity1, EnchantSlotType slotType1, String desc1)
    {
        name = name1;
        MaxLevel = MaxLevel1;
        rarity = rarity1;
        slotType = slotType1;
        desc = desc1;

    }

    public EnchantRarity getRarity()
    {
        return rarity;
    }

    public int getMaxLevel()
    {
        return MaxLevel;
    }

    public String getName()
    {
        return name;
    }

    public EnchantSlotType getSlotType()
    {
        return slotType;
    }

    public String getDesc()
    {
        return desc;
    }
}
