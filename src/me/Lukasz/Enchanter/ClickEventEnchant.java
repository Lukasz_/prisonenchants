package me.Lukasz.Enchanter;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.Enums.EnchantRarity;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.ItemBuilder;
import me.Lukasz.Utils.MathUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class ClickEventEnchant implements Listener
{

    private HashMap<UUID, ItemStack> enchantperson = new HashMap<UUID, ItemStack>();
    public EnchantUtils enchantUtils = new EnchantUtils();

    @EventHandler
    public void onInvEnchantClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
        Inventory inventory = event.getInventory();
        if(inventory.getName().equalsIgnoreCase(C.AquaB + "Enchanting"))
        {
            if(event.getCurrentItem()==null || event.getCurrentItem().getType()== Material.AIR)return;
            if (event.getAction()== InventoryAction.HOTBAR_MOVE_AND_READD || event.getAction()==InventoryAction.MOVE_TO_OTHER_INVENTORY||event.getAction()==InventoryAction.HOTBAR_SWAP||event.getClick().isKeyboardClick())
            {
                event.setCancelled(true);
            }
            event.setCancelled(true);
            ItemStack itemStack = event.getCurrentItem();
            if(event.getRawSlot()==13 && event.getCurrentItem()!=null)
            {
                player.getInventory().addItem(itemStack);
                inventory.setItem(13, new ItemStack(Material.AIR));
                enchantperson.remove(player.getUniqueId());
                updateInventory(inventory, new ItemStack(Material.AIR));
                return;
            }
            if(EnchantUtils.isValidEnchant(itemStack) && inventory.getItem(13)==null)
            {
                inventory.setItem(13, itemStack);
                event.setCurrentItem(new ItemStack(Material.AIR));
                enchantperson.put(player.getUniqueId(), itemStack);
                updateInventory(inventory, itemStack);
                return;
            }
            if(EnchantUtils.isValidEnchant(itemStack) && inventory.getItem(13)!=null)
            {
                player.getInventory().addItem(inventory.getItem(13));
                inventory.setItem(13, itemStack);
                event.setCurrentItem(new ItemStack(Material.AIR));
                enchantperson.put(player.getUniqueId(), itemStack);
                updateInventory(inventory, itemStack);
                return;
            }
            if(inventory.getItem(13) == null || inventory.getItem(13).getType() == Material.AIR)return;
            if(event.getCurrentItem().getType()==Material.REDSTONE_BLOCK)return;
            if(event.getRawSlot() == 28)
            {
                if(playerEconomy.hasAmount(getCost(event.getCurrentItem().getItemMeta()),PlayerEconomy.CURRENCY.STARDUST))
                {
                    inventory.setItem(13, enchantUtils.addEnchant(inventory.getItem(13), inventory, EnchantRarity.COMMON, player));
                    enchantperson.put(player.getUniqueId(), inventory.getItem(13));
                    playerEconomy.removeCurrency(getCost(event.getCurrentItem().getItemMeta()), PlayerEconomy.CURRENCY.STARDUST);
                    inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
                    updateInventory(inventory, inventory.getItem(13));
                    return;
                }
                else
                {
                    setItem(inventory, 28, inventory.getItem(28));
                    return;
                }
            }
            else if(event.getRawSlot() == 30)
            {
                if(playerEconomy.hasAmount(getCost(event.getCurrentItem().getItemMeta()),PlayerEconomy.CURRENCY.STARDUST))
                {
                    inventory.setItem(13, enchantUtils.addEnchant(inventory.getItem(13), inventory, EnchantRarity.UNIQUE, player));
                    enchantperson.put(player.getUniqueId(), inventory.getItem(13));
                    playerEconomy.removeCurrency(getCost(event.getCurrentItem().getItemMeta()), PlayerEconomy.CURRENCY.STARDUST);
                    inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
                    updateInventory(inventory, inventory.getItem(13));
                    return;
                }
                else
                {
                    setItem(inventory, 30, inventory.getItem(30));
                    return;
                }
            }
            else if(event.getRawSlot() == 32)
            {
                if(playerEconomy.hasAmount(getCost(event.getCurrentItem().getItemMeta()),PlayerEconomy.CURRENCY.STARDUST))
                {
                    inventory.setItem(13, enchantUtils.addEnchant(inventory.getItem(13), inventory, EnchantRarity.ELITE, player));
                    enchantperson.put(player.getUniqueId(), inventory.getItem(13));
                    playerEconomy.removeCurrency(getCost(event.getCurrentItem().getItemMeta()), PlayerEconomy.CURRENCY.STARDUST);
                    inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
                    updateInventory(inventory, inventory.getItem(13));
                    return;
                }
                else
                {
                    setItem(inventory, 32, inventory.getItem(32));
                    return;
                }
            }
            else if(event.getRawSlot() == 34)
            {
                if(playerEconomy.hasAmount(getCost(event.getCurrentItem().getItemMeta()),PlayerEconomy.CURRENCY.STARDUST))
                {
                    inventory.setItem(13, enchantUtils.addEnchant(inventory.getItem(13), inventory, EnchantRarity.LEGENDRY, player));
                    enchantperson.put(player.getUniqueId(), inventory.getItem(13));
                    playerEconomy.removeCurrency(getCost(event.getCurrentItem().getItemMeta()), PlayerEconomy.CURRENCY.STARDUST);
                    inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
                    updateInventory(inventory, inventory.getItem(13));
                    return;
                }
                else
                {
                    setItem(inventory, 34, inventory.getItem(34));
                    return;
                }
            }
            else if(event.getRawSlot() == 40)
            {
                if(playerEconomy.hasAmount(getCost(event.getCurrentItem().getItemMeta()),PlayerEconomy.CURRENCY.STARDUST))
                {
                    inventory.setItem(13, enchantUtils.addVanilaEnchant(inventory.getItem(13), inventory, player));
                    enchantperson.put(player.getUniqueId(), inventory.getItem(13));
                    playerEconomy.removeCurrency(getCost(event.getCurrentItem().getItemMeta()), PlayerEconomy.CURRENCY.STARDUST);
                    inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
                    updateInventory(inventory, inventory.getItem(13));
                    return;
                }
                else
                {
                    setItem(inventory, 40, inventory.getItem(40));
                    return;
                }
            }
        }
    }

    @EventHandler
    public void onCloseEnchantInv(InventoryCloseEvent event)
    {
        Player player = (Player) event.getPlayer();
        Inventory inventory = event.getInventory();
        if(inventory.getName().equalsIgnoreCase(C.AquaB + "Enchanting"))
        {
            if(enchantperson.containsKey(player.getUniqueId()))
            {
                player.getInventory().addItem(enchantperson.get(player.getUniqueId()));
                enchantperson.remove(player.getUniqueId());
            }
        }
    }

    private void updateInventory(Inventory inventory, ItemStack itemStack)
    {
        if(itemStack==null || itemStack.getType()==Material.AIR)
        {
            inventory.setItem(28, new ItemBuilder(Material.INK_SACK, (byte) 10).setName(C.GreenB + "Common Enchant").setLore("", C.GrayI + "Cost: N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
            inventory.setItem(30, new ItemBuilder(Material.INK_SACK, (byte) 11).setName(C.YellowB + "Unique Enchant").setLore("", C.GrayI + "Cost: N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
            inventory.setItem(32, new ItemBuilder(Material.INK_SACK, (byte) 12).setName(C.AquaB + "Elite Enchant").setLore("", C.GrayI + "Cost: N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
            inventory.setItem(34, new ItemBuilder(Material.INK_SACK, (byte) 14).setName(C.GoldB + "Legendary Enchant").setLore("", C.GrayI + "Cost: N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
            inventory.setItem(40, new ItemBuilder(Material.INK_SACK, (byte) 8).setName(C.GrayB + "Vanilla Enchant").setLore("", C.GrayI + "Cost: N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
        }
        else if(itemStack.getType()==Material.WOOD_PICKAXE || itemStack.getType()==Material.STONE_PICKAXE)
        {
            setItemValue(inventory, itemStack, 300, 600, 1200, 2500, 400);
        }
        else if(itemStack.getType()==Material.GOLD_PICKAXE || itemStack.getType()==Material.IRON_PICKAXE)
        {
            setItemValue(inventory, itemStack, 600, 1200, 2400, 5000, 800);
        }
        else if(itemStack.getType()==Material.DIAMOND_PICKAXE)
        {
            setItemValue(inventory, itemStack, 1200, 2400, 5000, 10000, 1600);
        }
        else if(itemStack.getType()==Material.FISHING_ROD)
        {
            setItemValue(inventory, itemStack, 300, 600, 1200, 2500, 400);
        }
        else if(itemStack.getType()==Material.WOOD_SWORD||itemStack.getType()==Material.LEATHER_HELMET||itemStack.getType()==Material.LEATHER_CHESTPLATE||itemStack.getType()==Material.LEATHER_LEGGINGS||itemStack.getType()==Material.LEATHER_BOOTS)
        {
            setItemValue(inventory,itemStack,350,700,1400,2900,600);
        }
        else if(itemStack.getType()==Material.GOLD_SWORD||itemStack.getType()==Material.GOLD_HELMET||itemStack.getType()==Material.GOLD_CHESTPLATE||itemStack.getType()==Material.GOLD_LEGGINGS||itemStack.getType()==Material.GOLD_BOOTS)
        {
            setItemValue(inventory,itemStack,400,800,1600,2400,800);
        }
        else if(itemStack.getType()==Material.STONE_SWORD||itemStack.getType()==Material.CHAINMAIL_HELMET||itemStack.getType()==Material.CHAINMAIL_CHESTPLATE||itemStack.getType()==Material.CHAINMAIL_LEGGINGS||itemStack.getType()==Material.CHAINMAIL_BOOTS)
        {
            setItemValue(inventory,itemStack,450,900,1800,3600,1000);
        }
        else if(itemStack.getType()==Material.IRON_SWORD||itemStack.getType()==Material.IRON_HELMET||itemStack.getType()==Material.IRON_CHESTPLATE||itemStack.getType()==Material.IRON_LEGGINGS||itemStack.getType()==Material.IRON_BOOTS)
        {
            setItemValue(inventory,itemStack,550,1100,2200,4400,1400);
        }
        else if(itemStack.getType()==Material.DIAMOND_SWORD||itemStack.getType()==Material.DIAMOND_HELMET||itemStack.getType()==Material.DIAMOND_CHESTPLATE||itemStack.getType()==Material.DIAMOND_LEGGINGS||itemStack.getType()==Material.DIAMOND_BOOTS)
        {
            setItemValue(inventory,itemStack,650,1300,2600,5200,1800);
        }
        else
        {
            setItemValue(inventory, itemStack, 99999,99999,99999,99999,99999);
        }
    }

    private void setItemValue(Inventory inventory, ItemStack itemStack, int common, int unique, int elite, int legendary, int vanilla)
    {
        inventory.setItem(28, new ItemBuilder(Material.INK_SACK, (byte) 10).setName(C.GreenB + "Common Enchant").setLore("", C.GrayI + "Cost: " + C.Gold + common +" (Star Dust)",C.GrayI + "Failure Rate: " + C.RedB + enchantUtils.getChanceOfEnchantFail(itemStack, EnchantRarity.COMMON) + "%","", C.GrayI + "Click to enchant").build());
        inventory.setItem(30, new ItemBuilder(Material.INK_SACK, (byte) 11).setName(C.YellowB + "Unique Enchant").setLore("", C.GrayI + "Cost: " + C.Gold + unique  +" (Star Dust)",C.GrayI + "Failure Rate: " + C.RedB + enchantUtils.getChanceOfEnchantFail(itemStack, EnchantRarity.UNIQUE) + "%","", C.GrayI + "Click to enchant").build());
        inventory.setItem(32, new ItemBuilder(Material.INK_SACK, (byte) 12).setName(C.AquaB + "Elite Enchant").setLore("", C.GrayI + "Cost: " + C.Gold + elite  +" (Star Dust)",C.GrayI + "Failure Rate: " + C.RedB + enchantUtils.getChanceOfEnchantFail(itemStack, EnchantRarity.ELITE) + "%","", C.GrayI + "Click to enchant").build());
        inventory.setItem(34, new ItemBuilder(Material.INK_SACK, (byte) 14).setName(C.GoldB + "Legendary Enchant").setLore("", C.GrayI + "Cost: " + C.Gold + legendary  +" (Star Dust)",C.GrayI + "Failure Rate: " + C.RedB + enchantUtils.getChanceOfEnchantFail(itemStack, EnchantRarity.LEGENDRY) + "%","", C.GrayI + "Click to enchant").build());
        inventory.setItem(40, new ItemBuilder(Material.INK_SACK, (byte) 8).setName(C.GrayB + "Vanilla Enchant").setLore("", C.GrayI + "Cost: " + C.Gold  + vanilla +" (Star Dust)",C.GrayI + "Failure Rate: " + C.RedB + enchantUtils.ChanceOFVanillaFail(itemStack) + "%","", C.GrayI + "Click to enchant").build());
    }

    private int getCost(ItemMeta itemMeta)
    {
        for(String string : itemMeta.getLore())
        {
            if(string.startsWith(C.GrayI + "Cost: "))
            {
                String cost = ChatColor.stripColor(string);
                cost = cost.replace("Cost: ", "");
                cost = cost.replace(" (Star Dust)", "");
                return Integer.parseInt(cost);
            }
        }
        return 99999;
    }

    private void setItem(Inventory inventory, int Slot, ItemStack prev)
    {
        inventory.setItem(Slot, new ItemBuilder(Material.REDSTONE_BLOCK).setName(C.RedB + "Not Enough Star Dust").build());
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                inventory.setItem(Slot, prev);
            }
        }.runTaskLaterAsynchronously(CustomEnchants.plugin, 40L);
    }

}
