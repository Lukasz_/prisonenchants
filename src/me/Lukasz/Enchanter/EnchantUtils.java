package me.Lukasz.Enchanter;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Enchanter.Enums.EnchantList;
import me.Lukasz.Enchanter.Enums.EnchantRarity;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class EnchantUtils
{

    public Random random = new Random();

    public static boolean isValidEnchant(ItemStack itemStack)
    {
        if (itemStack.getType().toString().contains("PICK") || itemStack.getType().name().contains("FISHING")|| itemStack.getType().name().contains("HELMET")||itemStack.getType().toString().contains("SWORD")||itemStack.getType().toString().contains("LEGGINGS")||itemStack.getType().toString().contains("BOOTS")||itemStack.getType().toString().contains("CHESTPLATE"))
            return true;
        return false;
    }

    /*
    This Method responds to how a Custom Enchant is added to an Item
    This only takes into account Vanilla Enchants!
    Simply Copy and Paste the info!
     */
    public ItemStack addVanilaEnchant(ItemStack added, Inventory inventory, Player player)
    {
        ItemStack itemStack = added;
        if (itemStack.getType().toString().contains("PICK"))
        {
            ArrayList<Enchantment> enchants = new ArrayList<>();
            enchants.add(Enchantment.DIG_SPEED);
            enchants.add(Enchantment.DURABILITY);
            if(itemStack.getEnchantments().size()==0)
            {
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(enchants.get(random.nextInt(enchants.size())), 1);
                return itemStack;
            }
            if(ChanceOFVanillaFail(itemStack)==100)
            {
                setItemsAround(inventory, player, false);
                return itemStack;
            }
            Enchantment newenchant = enchants.get(random.nextInt(enchants.size()));
            if (ChanceOFVanillaFail(itemStack) <= random.nextInt(100))
            {
                for(int i = 0; i < 100; i++)
                {
                    if(itemStack.getEnchantments().containsKey(newenchant))
                    {
                        if(newenchant.getMaxLevel()==itemStack.getEnchantmentLevel(newenchant))
                        {
                            if(enchants.size()!=1)
                            {
                                enchants.remove(newenchant);
                                newenchant = enchants.get(random.nextInt(enchants.size()));
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            setItemsAround(inventory,player,true);
                            itemStack.addEnchantment(newenchant, itemStack.getItemMeta().getEnchantLevel(newenchant)+1);
                            return itemStack;
                        }
                    }
                }
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(newenchant, 1);
                return itemStack;
            }
            else
            {
                setItemsAround(inventory,player,false);
                return itemStack;
            }
        }
        else if (itemStack.getType().toString().contains("FISHING"))
        {
            ArrayList<Enchantment> enchants = new ArrayList<>();
            enchants.add(Enchantment.LURE);
            enchants.add(Enchantment.DURABILITY);
            if(itemStack.getEnchantments().size()==0)
            {
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(enchants.get(random.nextInt(enchants.size())), 1);
                return itemStack;
            }
            if(ChanceOFVanillaFail(itemStack)==100)
            {
                setItemsAround(inventory, player, false);
                return itemStack;
            }
            Enchantment newenchant = enchants.get(random.nextInt(enchants.size()));
            if (ChanceOFVanillaFail(itemStack) <= random.nextInt(100))
            {
                for(int i = 0; i < 100; i++)
                {
                    if(itemStack.getEnchantments().containsKey(newenchant))
                    {
                        if(newenchant.getMaxLevel()==itemStack.getEnchantmentLevel(newenchant))
                        {
                            if(enchants.size()!=1)
                            {
                                enchants.remove(newenchant);
                                newenchant = enchants.get(random.nextInt(enchants.size()));
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            setItemsAround(inventory,player,true);
                            itemStack.addEnchantment(newenchant, itemStack.getItemMeta().getEnchantLevel(newenchant)+1);
                            return itemStack;
                        }
                    }
                }
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(newenchant, 1);
                return itemStack;
            }
            else
            {
                setItemsAround(inventory,player,false);
                return itemStack;
            }
        }
        else if (itemStack.getType().toString().contains("HELMET")||itemStack.getType().toString().contains("CHESTPLATE")||itemStack.getType().toString().contains("LEGGINGS"))
        {
            ArrayList<Enchantment> enchants = new ArrayList<>();
            enchants.add(Enchantment.PROTECTION_ENVIRONMENTAL);
            enchants.add(Enchantment.PROTECTION_PROJECTILE);
            enchants.add(Enchantment.PROTECTION_FIRE);
            enchants.add(Enchantment.DURABILITY);
            if(itemStack.getEnchantments().size()==0)
            {
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(enchants.get(random.nextInt(enchants.size())), 1);
                return itemStack;
            }
            if(ChanceOFVanillaFail(itemStack)==100)
            {
                setItemsAround(inventory, player, false);
                return itemStack;
            }
            Enchantment newenchant = enchants.get(random.nextInt(enchants.size()));
            if (ChanceOFVanillaFail(itemStack) <= random.nextInt(100))
            {
                for(int i = 0; i < 100; i++)
                {
                    if(itemStack.getEnchantments().containsKey(newenchant))
                    {
                        if(newenchant.getMaxLevel()==itemStack.getEnchantmentLevel(newenchant))
                        {
                            if(enchants.size()!=1)
                            {
                                enchants.remove(newenchant);
                                newenchant = enchants.get(random.nextInt(enchants.size()));
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            setItemsAround(inventory,player,true);
                            itemStack.addEnchantment(newenchant, itemStack.getItemMeta().getEnchantLevel(newenchant)+1);
                            return itemStack;
                        }
                    }
                }
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(newenchant, 1);
                return itemStack;
            }
            else
            {
                setItemsAround(inventory,player,false);
                return itemStack;
            }
        }
        else if (itemStack.getType().toString().contains("BOOTS"))
        {
            ArrayList<Enchantment> enchants = new ArrayList<>();
            enchants.add(Enchantment.PROTECTION_FALL);
            enchants.add(Enchantment.PROTECTION_ENVIRONMENTAL);
            enchants.add(Enchantment.PROTECTION_PROJECTILE);
            enchants.add(Enchantment.PROTECTION_FIRE);
            enchants.add(Enchantment.DURABILITY);
            if(itemStack.getEnchantments().size()==0)
            {
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(enchants.get(random.nextInt(enchants.size())), 1);
                return itemStack;
            }
            if(ChanceOFVanillaFail(itemStack)==100)
            {
                setItemsAround(inventory, player, false);
                return itemStack;
            }
            Enchantment newenchant = enchants.get(random.nextInt(enchants.size()));
            if (ChanceOFVanillaFail(itemStack) <= random.nextInt(100))
            {
                for(int i = 0; i < 100; i++)
                {
                    if(itemStack.getEnchantments().containsKey(newenchant))
                    {
                        if(newenchant.getMaxLevel()==itemStack.getEnchantmentLevel(newenchant))
                        {
                            if(enchants.size()!=1)
                            {
                                enchants.remove(newenchant);
                                newenchant = enchants.get(random.nextInt(enchants.size()));
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            setItemsAround(inventory,player,true);
                            itemStack.addEnchantment(newenchant, itemStack.getItemMeta().getEnchantLevel(newenchant)+1);
                            return itemStack;
                        }
                    }
                }
                setItemsAround(inventory,player,true);
                itemStack.addEnchantment(newenchant, 1);
                return itemStack;
            }
            else
            {
                setItemsAround(inventory,player,false);
                return itemStack;
            }
        }
        setItemsAround(inventory,player,false);
        return itemStack;
    }

    /*
    This method is responsibe for adding all the Enchants
    It's all done just need to add the "PICK" OR "FISHING" ETC TO IT
     */
    public ItemStack addEnchant(ItemStack added, Inventory inventory, EnchantRarity enchantRarity, Player player)
    {
        ItemStack itemStack = added;
        ArrayList<EnchantList> validEnchants = new ArrayList<>();
        for (EnchantList enchantList : EnchantList.values())
        {
            if (added.getType().name().contains("PICK"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.PICKAXE && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("FISHING"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.FISHING_ROD && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("HELMET"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.HELMET && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("CHESTPLATE"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.CHESTPLATE && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("LEGGINGS"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.LEGGINGS && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("BOOTS"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.BOOTS && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
            else if (added.getType().name().contains("SWORD"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.SWORD && enchantList.getRarity() == enchantRarity)
                {
                    validEnchants.add(enchantList);
                }
            }
        }
        if (validEnchants.size() == 0)
        {
            return itemStack;
        }
        EnchantList newEnchant = validEnchants.get(random.nextInt(validEnchants.size()));
        List<String> currentlore = itemStack.getItemMeta().getLore();
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (!itemStack.getItemMeta().hasLore())
        {
            setItemsAround(inventory, player, true);
            itemMeta.setLore(Arrays.asList(newEnchant.getRarity().getRarityColorFull() + newEnchant.getName() + " I"));
            itemStack.setItemMeta(itemMeta);
            return itemStack;
        }
        if(getChanceOfEnchantFail(itemStack, enchantRarity)==0)
        {
            setItemsAround(inventory, player, true);
            currentlore.add(newEnchant.getRarity().getRarityColorFull() + newEnchant.getName() + " I");
            itemMeta.setLore(currentlore);
            itemStack.setItemMeta(itemMeta);
            return itemStack;
        }
        if(getChanceOfEnchantFail(itemStack, enchantRarity)==100)
        {
            setItemsAround(inventory, player, false);
            return itemStack;
        }
        if (getChanceOfEnchantFail(itemStack, enchantRarity) <= random.nextInt(100))
        {
            int loreid = 0;
            for(int i = 0; i < 100; i++)
            {
                for (String lore : itemStack.getItemMeta().getLore())
                {
                    if (lore.startsWith(newEnchant.getRarity().getRarityColorFull() + newEnchant.getName()))
                    {
                        if (getLevel(lore, (newEnchant.getRarity().getRarityColorFull() + newEnchant.getName())) != newEnchant.getMaxLevel())
                        {
                            setItemsAround(inventory, player, true);
                            currentlore.set(loreid, newEnchant.getRarity().getRarityColorFull() + newEnchant.getName() + " " + getLevelRoman(getLevel(lore, (newEnchant.getRarity().getRarityColorFull() + newEnchant.getName())) + 1));
                            itemMeta.setLore(currentlore);
                            itemStack.setItemMeta(itemMeta);
                            return itemStack;
                        }
                        else if(validEnchants.size()!=1)
                        {
                            validEnchants.remove(newEnchant);
                            newEnchant = validEnchants.get(random.nextInt(validEnchants.size()));
                        }
                        else
                        {
                            break;
                        }
                    }
                    loreid++;
                }
            }
            setItemsAround(inventory, player, true);
            currentlore.add(newEnchant.getRarity().getRarityColorFull() + newEnchant.getName() + " I");
            itemMeta.setLore(currentlore);
            itemStack.setItemMeta(itemMeta);
            return itemStack;

        } else
        {
            setItemsAround(inventory, player, false);
            return itemStack;
        }
    }

    public int ChanceOFVanillaFail(ItemStack itemStack)
    {
        if (itemStack.getType().name().contains("PICK"))
        {
            int hasEnchant = 0;
            int maxEnchant = 8;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        else if (itemStack.getType().name().contains("FISHING"))
        {
            int hasEnchant = 0;
            int maxEnchant = 6;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        else if (itemStack.getType().name().contains("HELMET"))
        {
            int hasEnchant = 0;
            int maxEnchant = 15;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        else if (itemStack.getType().name().contains("CHESTPLATE"))
        {
            int hasEnchant = 0;
            int maxEnchant = 15;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        else if (itemStack.getType().name().contains("LEGGINGS"))
        {
            int hasEnchant = 0;
            int maxEnchant = 15;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        else if (itemStack.getType().name().contains("BOOTS"))
        {
            int hasEnchant = 0;
            int maxEnchant = 19;
            for(Enchantment enchantment : itemStack.getEnchantments().keySet())
            {
                hasEnchant = hasEnchant + itemStack.getItemMeta().getEnchantLevel(enchantment);
            }
            if (hasEnchant == maxEnchant)
            {
                return 100;
            }
            double num = ((double) hasEnchant) / ((double) maxEnchant);
            num = num * 100;
            return (int) num;
        }
        return 999;
    }

    public int getChanceOfEnchantFail(ItemStack itemStack, EnchantRarity enchantRarity)
    {
        int allenchants = 0;
        int allmaxlevels = 0;
        ArrayList<EnchantList> allEncahnts = new ArrayList<>();
        for (EnchantList enchantList : EnchantList.values())
        {
            if (itemStack.getType().name().contains("PICK"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.PICKAXE && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            } else if (itemStack.getType().name().contains("FISHING"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.FISHING_ROD && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
            else if (itemStack.getType().name().contains("HELMET"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.HELMET && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
            else if (itemStack.getType().name().contains("CHESTPLATE"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.CHESTPLATE && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
            else if (itemStack.getType().name().contains("LEGGINGS"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.LEGGINGS && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
            else if (itemStack.getType().name().contains("BOOTS"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.BOOTS && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
            else if (itemStack.getType().name().contains("SWORD"))
            {
                if (enchantList.getSlotType() == EnchantSlotType.SWORD && enchantList.getRarity() == enchantRarity)
                {
                    allEncahnts.add(enchantList);
                }
            }
        }
        if (allEncahnts.size() == 0)
        {
            return 100;
        }
        if (!itemStack.getItemMeta().hasLore())
        {
            return 0;
        }
        for (EnchantList enchantList : allEncahnts)
        {
            for (String lore : itemStack.getItemMeta().getLore())
            {
                if (lore.startsWith(enchantList.getRarity().getRarityColorFull() + enchantList.getName()))
                {
                    String enchant = ChatColor.stripColor(lore);
                    enchant = enchant.replace(enchantList.getName() + " ", "");
                    allenchants = allenchants + getLevel(enchant);
                }
            }
            allmaxlevels = allmaxlevels + enchantList.getMaxLevel();
        }
        if (allmaxlevels == allenchants)
        {
            return 100;
        }
        double num = ((double) allenchants) / ((double) allmaxlevels);
        num = num * 100;
        return (int) num;
    }

    private void setItemsAround(Inventory inventory, Player player, boolean success)
    {
        if (success)
        {
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2, 2);
            inventory.setItem(12, new ItemBuilder(Material.EMERALD_BLOCK).setName(C.GreenB + "Success").build());
            inventory.setItem(14, new ItemBuilder(Material.EMERALD_BLOCK).setName(C.GreenB + "Success").build());
            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    inventory.setItem(12, new ItemStack(Material.AIR));
                    inventory.setItem(14, new ItemStack(Material.AIR));
                }
            }.runTaskLaterAsynchronously(CustomEnchants.plugin, 40L);
        } else
        {
            player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_BREAK, 2, 2);
            inventory.setItem(12, new ItemBuilder(Material.REDSTONE_BLOCK).setName(C.RedB + "Unsuccessful").build());
            inventory.setItem(14, new ItemBuilder(Material.REDSTONE_BLOCK).setName(C.RedB + "Unsuccessful").build());
            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    inventory.setItem(12, new ItemStack(Material.AIR));
                    inventory.setItem(14, new ItemStack(Material.AIR));
                }
            }.runTaskLaterAsynchronously(CustomEnchants.plugin, 40L);
        }
    }

    public String getLevelRoman(int l)
    {
        if (l == 1)
        {
            return "I";
        }
        if (l == 2)
        {
            return "II";
        }
        if (l == 3)
        {
            return "III";
        }
        if (l == 4)
        {
            return "IV";
        }
        if (l == 5)
        {
            return "V";
        }
        if (l == 6)
        {
            return "VI";
        }
        if (l == 7)
        {
            return "VII";
        }
        if (l == 8)
        {
            return "VIII";
        }
        if (l == 9)
        {
            return "IX";
        }
        if (l == 10)
        {
            return "X";
        }
        return "0";
    }

    public int getLevel(String l)
    {
        if (l.equalsIgnoreCase("I"))
        {
            return 1;
        }
        if (l.equalsIgnoreCase("II"))
        {
            return 2;
        }
        if (l.equalsIgnoreCase("III"))
        {
            return 3;
        }
        if (l.equalsIgnoreCase("IV"))
        {
            return 4;
        }
        if (l.equalsIgnoreCase("V"))
        {
            return 5;
        }
        if (l.equalsIgnoreCase("VI"))
        {
            return 6;
        }
        if (l.equalsIgnoreCase("VII"))
        {
            return 7;
        }
        if (l.equalsIgnoreCase("VIII"))
        {
            return 8;
        }
        if (l.equalsIgnoreCase("IX"))
        {
            return 9;
        }
        if (l.equalsIgnoreCase("X"))
        {
            return 10;
        }
        return 0;
    }

    public int getLevel(String lore, String encahnt)
    {
        if (lore.equalsIgnoreCase(encahnt + " X"))
        {
            return 10;
        }
        if (lore.equalsIgnoreCase(encahnt + " IX"))
        {
            return 9;
        }
        if (lore.equalsIgnoreCase(encahnt + " VIII"))
        {
            return 8;
        }
        if (lore.equalsIgnoreCase(encahnt + " VII"))
        {
            return 7;
        }
        if (lore.equalsIgnoreCase(encahnt + " VI"))
        {
            return 6;
        }
        if (lore.equalsIgnoreCase(encahnt + " V"))
        {
            return 5;
        }
        if (lore.equalsIgnoreCase(encahnt + " IV"))
        {
            return 4;
        }
        if (lore.equalsIgnoreCase(encahnt + " III"))
        {
            return 3;
        }
        if (lore.equalsIgnoreCase(encahnt + " II"))
        {
            return 2;
        }
        if (lore.equalsIgnoreCase(encahnt + " I"))
        {
            return 1;
        }
        return 0;
    }

}
