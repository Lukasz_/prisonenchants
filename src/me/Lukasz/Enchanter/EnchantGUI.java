package me.Lukasz.Enchanter;

import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.InventoryUtils;
import me.Lukasz.Utils.ItemBuilder;
import me.Lukasz.Utils.MathUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EnchantGUI
{

    public Inventory createInv(PlayerEconomy playerEconomy)
    {
        Inventory inventory = Bukkit.createInventory(null, 54, C.AquaB + "Enchanting");
        inventory.setItem(13, new ItemStack(Material.AIR));
        inventory.setItem(0, new ItemBuilder(Material.BOOK).setName(C.AquaB + "Tutorial").setLore("", C.GrayI + "Welcome to the Enchanter! Here you can upgrade your items.", C.GrayI + "This allows you to gain special enchants for your items!", "", C.GrayI + "- Click the item you want to enchant.", C.GrayI + "- Select which rarity enchant you want to buy.", C.GrayI + "- Let the item enchant. If you wish to remove an item, click it.", "", C.RedI + "Warning: Not ALL enchants are successful!").build());
        inventory.setItem(28, new ItemBuilder(Material.INK_SACK, (byte) 10).setName(C.GreenB + "Common Enchant").setLore("", C.GrayI + "Cost: " + C.GoldB + "N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
        inventory.setItem(30, new ItemBuilder(Material.INK_SACK, (byte) 11).setName(C.YellowB + "Unique Enchant").setLore("", C.GrayI + "Cost: " + C.GoldB + "N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
        inventory.setItem(32, new ItemBuilder(Material.INK_SACK, (byte) 12).setName(C.AquaB + "Elite Enchant").setLore("", C.GrayI + "Cost: " + C.GoldB + "N/A(Star Dust)", C.GrayI + "Click to enchant!").build());
        inventory.setItem(34, new ItemBuilder(Material.INK_SACK, (byte) 14).setName(C.GoldB + "Legendary Enchant").setLore("", C.GrayI + "Cost: " + C.GoldB + "N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
        inventory.setItem(40, new ItemBuilder(Material.INK_SACK, (byte) 8).setName(C.GrayB + "Vanilla Enchant").setLore("", C.GrayI + "Cost: " + C.GoldB + "N/A (Star Dust)", C.GrayI + "Click to enchant!").build());
        inventory.setItem(53, new ItemBuilder(Material.PAPER).setName(C.WhiteB + "Star Dust: " + MathUtils.formatNumber(playerEconomy.getCurrentCurrency(PlayerEconomy.CURRENCY.STARDUST))).setLore("", C.GrayI + "- Your current Star Dust status!").build());
        InventoryUtils.fillInv(inventory);
        inventory.setItem(13, new ItemBuilder(Material.AIR).build());
        return inventory;
    }

}
