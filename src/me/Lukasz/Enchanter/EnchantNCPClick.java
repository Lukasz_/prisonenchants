package me.Lukasz.Enchanter;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.PrisonEconomy;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.MsgUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class EnchantNCPClick implements Listener
{

    EnchantGUI enchantGUI = new EnchantGUI();

    @EventHandler
    public void onNPCClick(PlayerInteractEntityEvent event)
    {
        Player player = event.getPlayer();
        PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
        EquipmentSlot hand = event.getHand();
        if (hand != null && !hand.equals(EquipmentSlot.HAND)) return;
        if (event.getRightClicked() instanceof Player)
        {
            switch ((((Player) event.getRightClicked()).getDisplayName()))
            {
                case "§b§lEnchanter":
                    player.openInventory(enchantGUI.createInv(playerEconomy));
                    new MsgUtils().sendMessagePrefixAndMessage(player, "Enchanter", "Opening Enchanter....");
                    break;
                case "§b§lBlacksmith":
                    player.sendMessage("");
                    new MsgUtils().sendMessagePrefixAndMessage(player, "BlackSmith", "Welcome to the Blacksmith!");
                    player.sendMessage("");
                    player.sendMessage(C.GrayI + "- Click any of the anvils to repair your item!");
                    player.sendMessage(C.GrayI + "- You will be asked to confirm your repair if you have enough Star Dust!");
                    player.sendMessage("");
                    break;
            }
        }
    }
}
