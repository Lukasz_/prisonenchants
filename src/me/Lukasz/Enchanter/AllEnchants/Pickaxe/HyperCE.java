package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class HyperCE extends newCustomEnchant
{

    public HyperCE()
    {
        super("Hyper", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        switch (ench)
        {
            case "Hyper I":
                if (random.nextInt(100) <= 10)
                {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 0));
                }
                break;
            case "Hyper II":
                if (random.nextInt(100) <= 15)
                {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 0));
                }
                break;
            case "Hyper III":
                if (random.nextInt(100) <= 20)
                {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 1));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }
}
