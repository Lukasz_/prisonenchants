package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.Booster.DustBooster;
import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import me.Lukasz.PrisonEconomy;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class LuckyCE extends newCustomEnchant
{

    public LuckyCE()
    {
        super("Lucky", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        switch (ench)
        {
            case "Hyper I":
                if (random.nextInt(100) <= 2)
                {
                    onMineBlockChance(player);
                }
                break;
            case "Hyper II":
                if (random.nextInt(100) <= 4)
                {
                    onMineBlockChance(player);
                }
                break;
            case "Hyper III":
                if (random.nextInt(100) <= 6)
                {
                    onMineBlockChance(player);
                }
                break;
            case "Hyper IV":
                if (random.nextInt(100) <= 8)
                {
                    onMineBlockChance(player);
                }
                break;
            case "Hyper V":
                if (random.nextInt(100) <= 10)
                {
                    onMineBlockChance(player);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    private void onMineBlockChance(Player player)
    {

        if (random.nextInt(500) == 345 || random.nextInt(500) == 23 || random.nextInt(500) == 432)
        {
            int given = random.nextInt(10) + 1;
            PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
            playerEconomy.addCurrency(given, PlayerEconomy.CURRENCY.MONEY);
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Lucky) §f" + "You have discovered " + given + " Coins!"));
            return;
        }
        if (random.nextInt(1000) == 654 || random.nextInt(1000) == 98 || random.nextInt(1000) == 864)
        {
            int given = random.nextInt(5) + 1;
            PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
            playerEconomy.addCurrency(given, PlayerEconomy.CURRENCY.STARS);
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Lucky) §f" + "You have discovered " + given + " Stars!"));
            return;
        }
        if (random.nextInt(250) <= 8)
        {
            DustBooster dustBooster = CustomEnchants.prisonCore.getEvents().getDustBooster();
            int i = random.nextInt(10) + 2;
            if(!(dustBooster==null))
            {
                i = i*dustBooster.getMulti();
            }
            PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
            playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Lucky) §f" + "You have discovered " + i + " Star Dust!"));
        }

    }
}