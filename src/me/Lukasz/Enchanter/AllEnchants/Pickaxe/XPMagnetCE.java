package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import me.Lukasz.Utils.MiningUtils.MineXP;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class XPMagnetCE extends newCustomEnchant
{

    public XPMagnetCE()
    {
        super("XP Magnet", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        MineXP mineXP = new MineXP(CustomEnchants.prisonCore.getPlayerData(player.getUniqueId()));
        switch (ench)
        {
            case "XP Magnet I":
                if (random.nextInt(100) <= 5)
                {
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(XP Magnet) §f" + "More xp added to your level!"));
                    mineXP.addXP(blockValueXP(block));
                }
                break;
            case "XP Magnet II":
                if (random.nextInt(100) <= 9)
                {
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(XP Magnet) §f" + "More xp added to your level!"));
                    mineXP.addXP(blockValueXP(block));
                }
                break;
            case "XP Magnet III":
                if (random.nextInt(100) <= 12)
                {
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(XP Magnet) §f" + "More xp added to your level!"));
                    mineXP.addXP(blockValueXP(block));
                }
                break;
            case "XP Magnet IV":
                if (random.nextInt(100) <= 19)
                {
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(XP Magnet) §f" + "More xp added to your level!"));
                    mineXP.addXP(blockValueXP(block));
                }
                break;
            case "XP Magnet V":
                if (random.nextInt(100) <= 22)
                {
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(XP Magnet) §f" + "More xp added to your level!"));
                    mineXP.addXP(blockValueXP(block));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    public int blockValueXP(Block block)
    {
        if (block.getType() == Material.COAL_ORE) return 1;
        if (block.getType() == Material.IRON_ORE) return 2;
        if (block.getType() == Material.GOLD_ORE) return 3;
        if (block.getType() == Material.GLOWING_REDSTONE_ORE) return 4;
        if (block.getType() == Material.LAPIS_ORE) return 7;
        if (block.getType() == Material.DIAMOND_ORE) return 10;
        if (block.getType() == Material.EMERALD_ORE) return 14;
        return 0;
    }
}
