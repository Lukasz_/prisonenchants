package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class CoinMagnetCE extends newCustomEnchant
{

    public CoinMagnetCE()
    {
        super("Coin Magnet", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        switch (ench)
        {
            case "Coin Magnet I":
                if (random.nextInt(100) <= 5)
                {
                    int i = random.nextInt(10) + 1;
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Coin Magnet) §f" + "You have discovered " + i + " Coins!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.MONEY);
                }
                break;
            case "Coin Magnet II":
                if (random.nextInt(100) <= 5)
                {
                    int i = random.nextInt(15) + 1;
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Coin Magnet) §f" + "You have discovered " + i + " Coins!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.MONEY);
                }
                break;
            case "Coin Magnet III":
                if (random.nextInt(100) <= 10)
                {
                    int i = random.nextInt(20) + 1;
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Coin Magnet) §f" + "You have discovered " + i + " Coins!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.MONEY);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
