package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.Booster.DustBooster;
import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class DustCollectorCE extends newCustomEnchant
{

    public DustCollectorCE()
    {
        super("Dust Collector", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        DustBooster dustBooster = CustomEnchants.prisonCore.getEvents().getDustBooster();
        switch (ench)
        {
            case "Dust Collector I":
                if (random.nextInt(100) <= 2)
                {
                    int i = random.nextInt(15) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector II":
                if (random.nextInt(100) <= 4)
                {
                    int i = random.nextInt(20) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector III":
                if (random.nextInt(100) <= 8)
                {
                    int i = random.nextInt(20) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector IV":
                if (random.nextInt(100) <= 12)
                {
                    int i = random.nextInt(25) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector V":
                if (random.nextInt(100) <= 17)
                {
                    int i = random.nextInt(30) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector VI":
                if (random.nextInt(100) <= 19)
                {
                    int i = random.nextInt(30) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector VII":
                if (random.nextInt(100) <= 20)
                {
                    int i = random.nextInt(35) + 1;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector VIII":
                if (random.nextInt(100) <= 20)
                {
                    int i = random.nextInt(35) + 5;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector IX":
                if (random.nextInt(100) <= 22)
                {
                    int i = random.nextInt(35) + 5;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            case "Dust Collector X":
                if (random.nextInt(100) <= 22)
                {
                    int i = random.nextInt(40) + 5;
                    if(!(dustBooster==null))
                    {
                        i = i*dustBooster.getMulti();
                    }
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Collector) §f" + "You have discovered " + i + " Star Dust!"));
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(i, PlayerEconomy.CURRENCY.STARDUST);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

}
