package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class FeedCE extends newCustomEnchant
{

    public FeedCE()
    {
        super("Feed", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        switch (ench)
        {
            case "Feed I":
                if (random.nextInt(100) <= 3)
                {
                    player.setFoodLevel(20);
                }
                break;
            case "Feed II":
                if (random.nextInt(100) <= 5)
                {
                    player.setFoodLevel(20);
                }
                break;
            case "Feed III":
                if (random.nextInt(100) <= 8)
                {
                    player.setFoodLevel(20);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }
}
