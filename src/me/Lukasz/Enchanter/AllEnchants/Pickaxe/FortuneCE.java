package me.Lukasz.Enchanter.AllEnchants.Pickaxe;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class FortuneCE extends newCustomEnchant
{

    public FortuneCE()
    {
        super("Fortune", EnchantSlotType.PICKAXE);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {
        switch (ench)
        {
            case "Fortune I":
                addBlocks(player, block, 1, 1);
                break;
            case "Fortune II":
                addBlocks(player, block, 2, 1);
                break;
            case "Fortune III":
                addBlocks(player, block, 1, 2);
                break;
            case "Fortune IV":
                addBlocks(player, block, 2, 2);
                break;
            case "Fortune V":
                addBlocks(player, block, 2, 3);
                break;
            case "Fortune VI":
                addBlocks(player, block, 3, 3);
                break;
            case "Fortune VII":
                addBlocks(player, block, 3, 4);
                break;
            case "Fortune VIII":
                addBlocks(player, block, 4, 4);
                break;
            case "Fortune IX":
                addBlocks(player, block, 4, 5);
                break;
            case "Fortune X":
                addBlocks(player, block, 5, 5);
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    private void addBlocks(Player player, Block block, int add, int multi)
    {
        if (player.getInventory().firstEmpty() != -1)
        {
            player.getWorld().dropItemNaturally(player.getLocation().add(0, 0.3, 0), new ItemStack(new ItemStack(block.getType() == Material.GLOWING_REDSTONE_ORE ? Material.REDSTONE_ORE : block.getType(), ((random.nextInt(blockAmount(block)) + add) * multi))));
        }
    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    private int blockAmount(Block block)
    {
        if (block.getType() == Material.COAL_ORE) return 1;
        if (block.getType() == Material.IRON_ORE) return 2;
        if (block.getType() == Material.GOLD_ORE) return 3;
        if (block.getType() == Material.GLOWING_REDSTONE_ORE) return 4;
        if (block.getType() == Material.LAPIS_ORE) return 5;
        if (block.getType() == Material.DIAMOND_ORE) return 6;
        if (block.getType() == Material.EMERALD_ORE) return 7;
        return 0;
    }

}