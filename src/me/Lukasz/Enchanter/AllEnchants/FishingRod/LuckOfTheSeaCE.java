package me.Lukasz.Enchanter.AllEnchants.FishingRod;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import me.Lukasz.Utils.Builders.ItemBuilder;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;

public class LuckOfTheSeaCE extends newCustomEnchant
{

    public LuckOfTheSeaCE()
    {
        super("Luck Of The Sea", EnchantSlotType.FISHING_ROD);
    }

    private Random random = new Random();

    @Override
    public void onCoughtFishing(String ench, Player player)
    {
        switch (ench)
        {
            case "Luck Of The Sea I":
                if (random.nextInt(100) <= 2)
                {
                    ItemStack itemStack = itemToGive();
                    int amount = itemStack.getAmount();
                    player.getInventory().addItem(itemStack);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Luck Of The Sea) §f" + "You have caught " + itemStack.getType().name() + " (x" + amount + ")"));
                }
                break;
            case "Luck Of The Sea II":
                if (random.nextInt(100) <= 4)
                {
                    ItemStack itemStack = itemToGive();
                    int amount = itemStack.getAmount();
                    player.getInventory().addItem(itemStack);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Luck Of The Sea) §f" + "You have caught " + itemStack.getType().name() + " (x" + amount + ")"));
                }
                break;
            case "Luck Of The Sea III":
                if (random.nextInt(100) <= 6)
                {
                    ItemStack itemStack = itemToGive();
                    int amount = itemStack.getAmount();
                    player.getInventory().addItem(itemStack);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Luck Of The Sea) §f" + "You have caught " + itemStack.getType().name() + " (x" + amount + ")"));
                }
                break;
            case "Luck Of The Sea IV":
                if (random.nextInt(100) <= 8)
                {
                    ItemStack itemStack = itemToGive();
                    int amount = itemStack.getAmount();
                    player.getInventory().addItem(itemStack);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Luck Of The Sea) §f" + "You have caught " + itemStack.getType().name() + " (x" + amount + ")"));
                }
                break;
            case "Luck Of The Sea V":
                if (random.nextInt(100) <= 10)
                {
                    ItemStack itemStack = itemToGive();
                    int amount = itemStack.getAmount();
                    player.getInventory().addItem(itemStack);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Luck Of The Sea) §f" + "You have caught " + itemStack.getType().name() + " (x" + amount + ")"));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }

    private ItemStack itemToGive()
    {
        ArrayList<ItemStack> items = new ArrayList<>();
        items.add(new ItemBuilder(Material.COAL_ORE).setAmount(random.nextInt(64) + 32).build());
        items.add(new ItemBuilder(Material.COAL_ORE).setAmount(random.nextInt(128) + 64).build());
        items.add(new ItemBuilder(Material.IRON_ORE).setAmount(random.nextInt(64) + 64).build());
        items.add(new ItemBuilder(Material.IRON_ORE).setAmount(random.nextInt(256) + 128).build());
        items.add(new ItemBuilder(Material.REDSTONE_ORE).setAmount(random.nextInt(64) + 64).build());
        items.add(new ItemBuilder(Material.REDSTONE_ORE).setAmount(random.nextInt(128) + 128).build());
        items.add(new ItemBuilder(Material.GOLD_ORE).setAmount(random.nextInt(128) + 128).build());
        items.add(new ItemBuilder(Material.GOLD_ORE).setAmount(random.nextInt(256) + 64).build());
        items.add(new ItemBuilder(Material.LAPIS_ORE).setAmount(random.nextInt(128) + 256).build());
        items.add(new ItemBuilder(Material.LAPIS_ORE).setAmount(random.nextInt(128) + 128).build());
        items.add(new ItemBuilder(Material.DIAMOND_ORE).setAmount(random.nextInt(64) + 64).build());
        items.add(new ItemBuilder(Material.DIAMOND_ORE).setAmount(random.nextInt(64) + 128).build());
        items.add(new ItemBuilder(Material.EMERALD_ORE).setAmount(random.nextInt(32) + 32).build());
        items.add(new ItemBuilder(Material.EMERALD_ORE).setAmount(random.nextInt(64) + 64).build());
        items.add(new ItemBuilder(Material.EMERALD_ORE).setAmount(random.nextInt(128) + 128).build());
        items.add(new ItemBuilder(Material.STONE_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.STONE_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.GOLD_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.GOLD_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.GOLD_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.DIAMOND_PICKAXE).setAmount(1).build());
        items.add(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setAmount(1).build());
        items.add(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setAmount(1).build());
        items.add(new ItemBuilder(Material.IRON_CHESTPLATE).setAmount(1).build());
        items.add(new ItemBuilder(Material.IRON_LEGGINGS).setAmount(1).build());
        items.add(new ItemBuilder(Material.IRON_HELMET).setAmount(1).build());
        items.add(new ItemBuilder(Material.DIAMOND_HELMET).setAmount(1).build());
        items.add(new ItemBuilder(Material.CHAINMAIL_LEGGINGS).setAmount(1).build());
        return items.get(random.nextInt(items.size()));
    }

}