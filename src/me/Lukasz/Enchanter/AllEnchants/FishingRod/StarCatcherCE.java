package me.Lukasz.Enchanter.AllEnchants.FishingRod;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class StarCatcherCE extends newCustomEnchant
{

    public StarCatcherCE()
    {
        super("Star Catcher", EnchantSlotType.FISHING_ROD);
    }

    private Random random = new Random();

    @Override
    public void onCoughtFishing(String ench, Player player)
    {
        switch (ench)
        {
            case "Star Catcher I":
                if (random.nextInt(100) <= 2)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(1, PlayerEconomy.CURRENCY.STARS);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Star Catcher) §f" + "You have caught 1 Star!"));}
                break;
            case "Star Catcher II":
                if (random.nextInt(100) <= 3)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(1, PlayerEconomy.CURRENCY.STARS);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Star Catcher) §f" + "You have caught 1 Star!"));}
                break;
            case "Star Catcher III":
                if (random.nextInt(100) <= 4)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(1, PlayerEconomy.CURRENCY.STARS);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Star Catcher) §f" + "You have caught 1 Star!"));}
                break;
            case "Star Catcher IV":
                if (random.nextInt(100) <= 5)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    playerEconomy.addCurrency(2, PlayerEconomy.CURRENCY.STARS);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Star Catcher) §f" + "You have caught 2 Star!"));}
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
