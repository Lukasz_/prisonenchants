package me.Lukasz.Enchanter.AllEnchants.FishingRod;

import me.Lukasz.Booster.DustBooster;
import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class DustCatcherCE extends newCustomEnchant
{

    public DustCatcherCE()
    {
        super("Dust Catcher", EnchantSlotType.FISHING_ROD);
    }

    private Random random = new Random();

    @Override
    public void onCoughtFishing(String ench, Player player)
    {
        DustBooster dustBooster = CustomEnchants.prisonCore.getEvents().getDustBooster();
        switch (ench)
        {
            case "Dust Catcher I":
                if (random.nextInt(100) <= 2)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(10) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));
                }
                break;
            case "Dust Catcher II":
                if (random.nextInt(100) <= 4)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(12) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));
                }
                break;
            case "Dust Catcher III":
                if (random.nextInt(100) <= 6)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(14) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));}
                break;
            case "Dust Catcher IV":
                if (random.nextInt(100) <= 8)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(16) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));}
                break;
            case "Dust Catcher V":
                if (random.nextInt(100) <= 10)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(18) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));}
                break;
            case "Dust Catcher VI":
                if (random.nextInt(100) <= 12)
                {
                    PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
                    int dust = random.nextInt(20) + 2;
                    if(!(dustBooster==null))
                    {
                        dust = dust*dustBooster.getMulti();
                    }
                    playerEconomy.addCurrency(dust, PlayerEconomy.CURRENCY.STARDUST);
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§b(Dust Catcher) §f" + "You have caught " + dust + " Star Dust!"));}
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {

    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
