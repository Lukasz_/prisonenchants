package me.Lukasz.Enchanter.AllEnchants.Armour.Chestplate;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class MoltenCE extends newCustomEnchant
{

    public MoltenCE()
    {
        super("Molten", EnchantSlotType.CHESTPLATE);
    }

    private Random random = new Random();

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {
    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {

    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

        switch (ench)
        {
            case "Molten I":
                if(random.nextInt(100) <= 2)
                {
                    dealingDamage.setFireTicks(40);
                }
                break;
            case "Molten II":
                if(random.nextInt(100) <= 4)
                {
                    dealingDamage.setFireTicks(40);
                }
                break;
            case "Molten III":
                if(random.nextInt(100) <= 6)
                {
                    dealingDamage.setFireTicks(80);
                }
                break;
            case "Molten IV":
                if(random.nextInt(100) <= 8)
                {
                    dealingDamage.setFireTicks(80);
                }
                break;
            case "Molten V":
                if(random.nextInt(100) <= 10)
                {
                    dealingDamage.setFireTicks(120);
                }
                break;
            case "Molten VI":
                if(random.nextInt(100) <= 12)
                {
                    dealingDamage.setFireTicks(200);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
