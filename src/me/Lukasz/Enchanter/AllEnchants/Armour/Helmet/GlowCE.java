package me.Lukasz.Enchanter.AllEnchants.Armour.Helmet;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GlowCE extends newCustomEnchant
{

    public GlowCE()
    {
        super("Glow", EnchantSlotType.HELMET);
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 99999999, (byte) 0.5));
    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {
        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
