package me.Lukasz.Enchanter.AllEnchants.Armour.Boots;

import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class LeapCE extends newCustomEnchant
{

    public LeapCE()
    {
        super("Leap", EnchantSlotType.BOOTS);
    }

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {
        switch (ench)
        {
            case "Leap I":
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999999, (byte) 0.5));
                break;
            case "Leap II":
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999999, (byte) 1));
                break;
            case "Leap III":
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999999, (byte) 2));
                break;
            default:
                break;
        }
    }

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {
        player.removePotionEffect(PotionEffectType.JUMP);
    }

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {

    }

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {

    }

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {

    }

    @Override
    public void onCoughtFishing(String ench, Player player)
    {

    }

    @Override
    public void onWaitingFishing(String ench, Player player)
    {

    }
}
