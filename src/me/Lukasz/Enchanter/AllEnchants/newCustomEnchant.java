package me.Lukasz.Enchanter.AllEnchants;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Enchanter.AllEnchants.Armour.Boots.LeapCE;
import me.Lukasz.Enchanter.AllEnchants.Armour.Chestplate.MoltenCE;
import me.Lukasz.Enchanter.AllEnchants.Armour.Helmet.GlowCE;
import me.Lukasz.Enchanter.AllEnchants.FishingRod.DustCatcherCE;
import me.Lukasz.Enchanter.AllEnchants.FishingRod.LuckOfTheSeaCE;
import me.Lukasz.Enchanter.AllEnchants.FishingRod.StarCatcherCE;
import me.Lukasz.Enchanter.AllEnchants.Pickaxe.*;
import me.Lukasz.Enchanter.Enums.EnchantSlotType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;

public abstract class newCustomEnchant implements Listener
{

    private String _encahnt;
    private EnchantSlotType _enchantmentSlotType;

    public newCustomEnchant(String enchant, EnchantSlotType enchantmentSlotType)
    {
        _encahnt = enchant;
        _enchantmentSlotType = enchantmentSlotType;
        Bukkit.getPluginManager().registerEvents(this, CustomEnchants.plugin);
    }

    public static void registerEnchants()
    {
        //Helmet
        new GlowCE();
        //Chestplate
        new MoltenCE();
        //Leggings
        //Boots
        new LeapCE();
        //Pickaxe
        new AdrenalinCE();
        new CoinMagnetCE();
        new DustCollectorCE();
        new FeedCE();
        new FortuneCE();
        new HyperCE();
        new LuckyCE();
        new XPMagnetCE();
        //Sword
        //Rod
        new LuckOfTheSeaCE();
        new DustCatcherCE();
        new StarCatcherCE();
    }

    /*
    Below are Events/Abstract Classes that deal with Rods!
     */

    public abstract void onCoughtFishing(String ench, Player player);

    public abstract void onWaitingFishing(String ench, Player player);

    @EventHandler
    public void FishingEvent(PlayerFishEvent event)
    {
        if(event.isCancelled())return;
        if(_enchantmentSlotType!=EnchantSlotType.FISHING_ROD)return;
        Player player = event.getPlayer();
        if(!(player.getInventory().getItemInMainHand().hasItemMeta())||!(player.getInventory().getItemInMainHand().getItemMeta().hasLore()))return;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        if(event.getState()== PlayerFishEvent.State.CAUGHT_ENTITY||event.getState()== PlayerFishEvent.State.CAUGHT_FISH)
        {
            if(event.getCaught()instanceof Player)return;
            for(String lore : itemStack.getItemMeta().getLore())
            {
                lore = ChatColor.stripColor(lore);
                if(lore.startsWith(_encahnt))
                {
                    onCoughtFishing(lore, player);
                }
            }
        }
        else if(event.getState()==PlayerFishEvent.State.FISHING&&!(event.getState()==PlayerFishEvent.State.IN_GROUND))
        {
            for(String lore : itemStack.getItemMeta().getLore())
            {
                lore = ChatColor.stripColor(lore);
                if(lore.startsWith(_encahnt))
                {
                    onWaitingFishing(lore, player);
                }
            }
        }
        else
        {
            return;
        }
    }

    /*
    Below are Events/Abstract classes that deal with Pickaxe Events
     */

    public abstract void onBlockMineEnchant(String ench, Player player, Block block);

    @EventHandler (priority = EventPriority.MONITOR)
    public void onBlockBreakEnchant(BlockBreakEvent event)
    {
        if(event.isCancelled())return;
        if(_enchantmentSlotType!=EnchantSlotType.PICKAXE)return;
        Player player = event.getPlayer();
        if(player.getInventory().getItemInMainHand()==null||player.getInventory().getItemInMainHand().getType()==Material.AIR)return;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
        {
            for(String lore : itemStack.getItemMeta().getLore())
            {
                lore = ChatColor.stripColor(lore);
                if(lore.startsWith(_encahnt))
                {
                    onBlockMineEnchant(lore, player, event.getBlock());
                }
            }
        }
    }
    /*
    Below are Events/Abstract classes that deal with Sword Damage
    Assigned on Hitting Players, returns Enchants + Damagers & Taking Damage
     */
    public abstract void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage);

    @EventHandler (priority = EventPriority.HIGH)
    public void onPlayerAttackCustomEnchant(EntityDamageByEntityEvent event)
    {
        if(event.isCancelled())return;
        if(_enchantmentSlotType!=EnchantSlotType.SWORD)return;
        if(event.getDamager()instanceof Player)
        {
            Player damager = (Player) event.getDamager();
            if(_enchantmentSlotType==EnchantSlotType.SWORD&&damager.getInventory().getItemInMainHand()!=null)
            {
                ItemStack itemStack = damager.getInventory().getItemInMainHand();
                if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
                {
                    for(String lore : itemStack.getItemMeta().getLore())
                    {
                        lore = ChatColor.stripColor(lore);
                        if(lore.startsWith(_encahnt))
                        {
                            onPlayerSwordDamage(lore, event.getEntity(), damager);
                        }
                    }
                }
            }
        }
    }

    /*
    Below are Events/Abstract Classes that deal with Hits
    When a Player X is hit, the Damager gets Effect/Enchants back
     */

    public abstract void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage);

    @EventHandler (priority = EventPriority.HIGH)
    public void PlayerTakeDamageEvent(EntityDamageByEntityEvent event)
    {
        if(event.isCancelled())return;
        if((_enchantmentSlotType!=EnchantSlotType.HELMET)||(_enchantmentSlotType!=EnchantSlotType.CHESTPLATE)||(_enchantmentSlotType!=EnchantSlotType.LEGGINGS)||(_enchantmentSlotType!=EnchantSlotType.BOOTS))
        if(event.getEntity()instanceof Player)
        {
            Player player = (Player) event.getEntity();
            if(_enchantmentSlotType==EnchantSlotType.HELMET&&player.getInventory().getHelmet()!=null)
            {
                ItemStack itemStack = player.getInventory().getHelmet();
                if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
                {
                    for(String lore : itemStack.getItemMeta().getLore())
                    {
                        lore = ChatColor.stripColor(lore);
                        if(lore.startsWith(_encahnt))
                        {
                            onPlayerDamageArmour(lore, player, event.getDamager());
                        }
                    }
                }
            }
            else if(_enchantmentSlotType==EnchantSlotType.CHESTPLATE&&player.getInventory().getChestplate()!=null)
            {
                ItemStack itemStack = player.getInventory().getChestplate();
                if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
                {
                    for(String lore : itemStack.getItemMeta().getLore())
                    {
                        lore = ChatColor.stripColor(lore);
                        if(lore.startsWith(_encahnt))
                        {
                            onPlayerDamageArmour(lore, player, event.getDamager());
                        }
                    }
                }
            }
            else if(_enchantmentSlotType==EnchantSlotType.LEGGINGS&&player.getInventory().getLeggings()!=null)
            {
                ItemStack itemStack = player.getInventory().getLeggings();
                if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
                {
                    for(String lore : itemStack.getItemMeta().getLore())
                    {
                        lore = ChatColor.stripColor(lore);
                        if(lore.startsWith(_encahnt))
                        {
                            onPlayerDamageArmour(lore, player, event.getDamager());
                        }
                    }
                }
            }
            else if(_enchantmentSlotType==EnchantSlotType.BOOTS&&player.getInventory().getBoots()!=null)
            {
                ItemStack itemStack = player.getInventory().getBoots();
                if(itemStack.hasItemMeta()&&itemStack.getItemMeta().hasLore())
                {
                    for(String lore : itemStack.getItemMeta().getLore())
                    {
                        lore = ChatColor.stripColor(lore);
                        if(lore.startsWith(_encahnt))
                        {
                            onPlayerDamageArmour(lore, player, event.getDamager());
                        }
                    }
                }
            }
        }
    }

    /*
    Below are Events/Abstract Classes that deal with Potion Effects
    When someone puts on a pice of Armour!
     */
    public abstract void onArmourAddPotionEffect(String ench, Player player);

    public abstract void onArmourRemovePotionEffect(Player player);

    @EventHandler  (priority = EventPriority.HIGH)
    public void ArmourListerePotion(InventoryClickEvent event)
    {
        if(event.isCancelled())return;
        if((_enchantmentSlotType!=EnchantSlotType.HELMET)||(_enchantmentSlotType!=EnchantSlotType.CHESTPLATE)||(_enchantmentSlotType!=EnchantSlotType.LEGGINGS)||(_enchantmentSlotType!=EnchantSlotType.BOOTS))
            if (event.getSlotType() == InventoryType.SlotType.ARMOR)
        {
            if (event.getClick() == ClickType.LEFT)
            {
                ItemStack onHelmet = event.getCursor();
                if (event.getRawSlot() == 5 && _enchantmentSlotType == EnchantSlotType.HELMET)
                {
                    onArmourRemovePotionEffect((Player) event.getWhoClicked());
                    if (onHelmet != null || onHelmet.getType() != Material.AIR)
                    {
                        if (onHelmet.hasItemMeta() && onHelmet.getItemMeta().hasLore())
                        {
                            for (String lore : onHelmet.getItemMeta().getLore())
                            {
                                lore = ChatColor.stripColor(lore);
                                if (lore.startsWith(_encahnt))
                                {
                                    onArmourAddPotionEffect(lore, (Player) event.getWhoClicked());
                                }
                            }
                        }
                    }
                }
                else if (event.getRawSlot() == 6 && _enchantmentSlotType == EnchantSlotType.CHESTPLATE)
                {
                    onArmourRemovePotionEffect((Player) event.getWhoClicked());
                    if (onHelmet != null || onHelmet.getType() != Material.AIR)
                    {
                        if (onHelmet.hasItemMeta() && onHelmet.getItemMeta().hasLore())
                        {
                            for (String lore : onHelmet.getItemMeta().getLore())
                            {
                                lore = ChatColor.stripColor(lore);
                                if (lore.startsWith(_encahnt))
                                {
                                    onArmourAddPotionEffect(lore, (Player) event.getWhoClicked());
                                }
                            }
                        }
                    }
                }
                else if (event.getRawSlot() == 7 && _enchantmentSlotType == EnchantSlotType.LEGGINGS)
                {
                    onArmourRemovePotionEffect((Player) event.getWhoClicked());
                    if (onHelmet != null || onHelmet.getType() != Material.AIR)
                    {
                        if (onHelmet.hasItemMeta() && onHelmet.getItemMeta().hasLore())
                        {
                            for (String lore : onHelmet.getItemMeta().getLore())
                            {
                                lore = ChatColor.stripColor(lore);
                                if (lore.startsWith(_encahnt))
                                {
                                    onArmourAddPotionEffect(lore, (Player) event.getWhoClicked());
                                }
                            }
                        }
                    }
                }
                else if (event.getRawSlot() == 8 && _enchantmentSlotType == EnchantSlotType.BOOTS)
                {
                    onArmourRemovePotionEffect((Player) event.getWhoClicked());
                    if (onHelmet != null || onHelmet.getType() != Material.AIR)
                    {
                        if (onHelmet.hasItemMeta() && onHelmet.getItemMeta().hasLore())
                        {
                            for (String lore : onHelmet.getItemMeta().getLore())
                            {
                                lore = ChatColor.stripColor(lore);
                                if (lore.startsWith(_encahnt))
                                {
                                    onArmourAddPotionEffect(lore, (Player) event.getWhoClicked());
                                }
                            }
                        }
                    }
                }
            } else
            {
                event.setCancelled(true);
            }
        }
    }

    /*

    COPY AND PASTE FOR NEW ENCHANT

    public NAME()
    {
        super("NAME OF ENCHANT", EnchantSlotType.SLOT);
    }

    private Random random = new Random();

    @Override
    public void onBlockMineEnchant(String ench, Player player, Block block)
    {}

    @Override
    public void onCoughtFishing(String ench, Player player)
    {}

    @Override
    public void onWaitingFishing(String ench, Player player)
    {}

    @Override
    public void onArmourAddPotionEffect(String ench, Player player)
    {}

    @Override
    public void onArmourRemovePotionEffect(Player player)
    {}

    @Override
    public void onPlayerDamageArmour(String ench, Player takingDamage, Entity dealingDamage)
    {}

    @Override
    public void onPlayerSwordDamage(String ench, Entity takingDamage, Player dealingDamage)
    {}

     */
}


