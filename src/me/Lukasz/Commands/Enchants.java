package me.Lukasz.Commands;

import me.Lukasz.Enchanter.Enums.EnchantList;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.MsgUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Enchants implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args)
    {
        MsgUtils msgUtils = new MsgUtils();
        Player player = (Player) sender;
        msgUtils.sendMessagePrefixAndMessage(player, "Enchants", "Here is the list of all Enchants!");
        player.sendMessage("");
        for(EnchantList enchantList : EnchantList.values())
        {
            player.sendMessage(enchantList.getRarity().getRarityColorFull() + enchantList.getName().toUpperCase() + C.GrayB +  " » " + C.GrayI + enchantList.getDesc() + " (" + new MsgUtils().firstCaptialLetter(enchantList.getSlotType().name()) + ")");
        }
        player.sendMessage("");
        return false;
    }
}
