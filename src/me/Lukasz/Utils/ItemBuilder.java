package me.Lukasz.Utils;

import me.Lukasz.Utils.C;
import me.Lukasz.Utils.Glow;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public ItemBuilder(Material material)
    {
        itemStack = new ItemStack(material);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder(Material material, byte data)
    {
        itemStack = new ItemStack(material, 1, data);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder setName(String name)
    {
        itemMeta.setDisplayName(C.translate(name));
        return this;
    }

    public ItemBuilder setLore(String... lore)
    {
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    public ItemBuilder setAmount(int amount)
    {
        itemStack.setAmount(amount);
        return this;
    }

    public ItemStack build()
    {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
