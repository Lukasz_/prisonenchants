package me.Lukasz;

import me.Lukasz.Commands.Enchants;
import me.Lukasz.Enchanter.ClickEventEnchant;
import me.Lukasz.Enchanter.EnchantNCPClick;
import me.Lukasz.Enchanter.AllEnchants.newCustomEnchant;
import me.Lukasz.Blacksmith.AnvilClick;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomEnchants extends JavaPlugin
{

    public static CustomEnchants plugin;
    public static PrisonCore prisonCore;
    public PrisonEconomy prisonEconomy;

    @Override
    public void onEnable()
    {
        plugin = this;
        prisonEconomy = (PrisonEconomy) this.getServer().getPluginManager().getPlugin("StarPrisonEconomy");
        prisonCore = (PrisonCore) this.getServer().getPluginManager().getPlugin("StarPrisonCore");
        newCustomEnchant.registerEnchants();
        //
        this.getServer().getPluginManager().registerEvents(new EnchantNCPClick(), this);
        this.getServer().getPluginManager().registerEvents(new ClickEventEnchant(), this);
        this.getServer().getPluginManager().registerEvents(new AnvilClick(), this);
        //
        this.getCommand("Enchants").setExecutor(new Enchants());
    }

    @Override
    public void onDisable()
    {

    }

}
