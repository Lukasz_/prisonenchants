package me.Lukasz.Blacksmith;

import me.Lukasz.CustomEnchants;
import me.Lukasz.Database.PlayerEconomy;
import me.Lukasz.Enchanter.EnchantUtils;
import me.Lukasz.Utils.C;
import me.Lukasz.Utils.MsgUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.UUID;

public class AnvilClick implements Listener
{

    private ArrayList<UUID> confirmPlayer = new ArrayList<>();

    @EventHandler
    public void onAnvildClick(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), CustomEnchants.plugin.prisonEconomy);
        MsgUtils msgUtils = new MsgUtils();
        if(event.getAction()== Action.RIGHT_CLICK_BLOCK)
        {
            if (event.getClickedBlock().getType() == Material.ANVIL)
            {
                if(event.getHand()== EquipmentSlot.OFF_HAND)return;
                event.setCancelled(true);
                if (player.getInventory().getItemInMainHand() == null || player.getInventory().getItemInMainHand().getType() == Material.AIR)
                    return;
                ItemStack itemStack = player.getInventory().getItemInMainHand();
                if(!EnchantUtils.isValidEnchant(itemStack))
                {
                    msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Sorry, you can't repair this item!");
                    return;
                }
                if (itemStack.getDurability() == 0)
                {
                    msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Sorry, your item has full durability!");
                    return;
                }
                if (confirmPlayer.contains(player.getUniqueId()))
                {
                    int cost = (itemStack.getDurability()) * 2;
                    if(playerEconomy.hasAmount(cost, PlayerEconomy.CURRENCY.STARDUST))
                    {
                        playerEconomy.removeCurrency(cost, PlayerEconomy.CURRENCY.STARDUST);
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Item has been Repaired!");
                        itemStack.setDurability((short) 0);
                        player.getInventory().setItemInMainHand(itemStack);
                        confirmPlayer.remove(player.getUniqueId());
                        return;
                    }
                    else
                    {
                        msgUtils.sendErrorMessage(player, "You do not have enough StarDust to repair this item!");
                        confirmPlayer.remove(player.getUniqueId());
                        return;
                    }
                }
                else
                {
                    int cost = (itemStack.getDurability()) * 2;
                    if (playerEconomy.hasAmount(cost, PlayerEconomy.CURRENCY.STARDUST))
                    {
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Cost of repair " + cost + " Star Dust!");
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Click again to " + C.GreenB + "CONFIRM" + C.White + " the repair!");
                        player.sendMessage("");
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", C.RedB + "WARNING: " + C.RedI + "You have 5 Seconds to confirm this repair!");
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", C.RedB + "WARNING: " + C.RedI + "Switching to another item could break the cost calculation!");
                        confirmPlayer.add(player.getUniqueId());
                        removeFromConfirmPlayers(player.getUniqueId());
                        return;
                    } else
                    {
                        msgUtils.sendMessagePrefixAndMessage(player, "Blacksmith", "Sorry you do not have enough to start the Repair! (" + cost + " Star Dust)");
                        return;
                    }
                }
            }
        }
    }

    public void removeFromConfirmPlayers(UUID uuid)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if(confirmPlayer.contains(uuid))
                {
                    confirmPlayer.remove(uuid);
                }
            }
        }.runTaskLater(CustomEnchants.plugin, 20L*5);
    }

}
